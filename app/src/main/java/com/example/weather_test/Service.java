package com.example.weather_test;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.weather_test.Constants.LOCATION_WEATHER_URL;

public class Service implements Response.Listener<JSONObject>, Response.ErrorListener {

    private Handler handler;
    private static Service service;

    private Service() {
    }

    public static Service getInstance() {
        if (null == service) {
            service = new Service();
        }
        return service;
    }

    public void getData(Context context, String location, final Handler handler) {
        this.handler = handler;
        String url = String.format(LOCATION_WEATHER_URL, location);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET, url, null, this, this);
        requestQueue.add(jsonObjectRequest);
    }

    private static void parseData(JSONObject response, List<WeatherItem> movieList) {
        try {
            // Get the JSON array
            // Parsing only needed items
            JSONArray array = response.getJSONArray("list");
            int len = array.length();
            // Loop through the array elements
            for (int i = 0; i < len; i++) {
                JSONObject obj = array.getJSONObject(i);
                JSONArray weather = obj.getJSONArray("weather");
                JSONObject weatherData = weather.getJSONObject(0);
                String main = weatherData.getString("main");
                String description = weatherData.getString("description");

                JSONObject windObj = obj.getJSONObject("wind");
                String windSpeed = windObj.getString("speed");

                WeatherItem weatherItem = new WeatherItem(main, description, windSpeed);
                movieList.add(weatherItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        List<WeatherItem> weatherItemsList;

        if (null == response) {
            Message message = new Message();
            message.obj = response;
            handler.sendMessage(message);
            return;
        }

        weatherItemsList = new ArrayList<>();
        parseData(response, weatherItemsList);
        sendDataBack(weatherItemsList);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        sendDataBack(null);
    }

    private void sendDataBack(List<WeatherItem> weatherItemsList) {
        Message message = new Message();
        message.obj = weatherItemsList;
        handler.sendMessage(message);
    }

}
