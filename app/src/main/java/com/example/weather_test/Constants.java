package com.example.weather_test;

public class Constants {

    public static final String API_KEY = "5576db381a6fba90c6ec2747e5d335b2";
    public static final String LOCATION_WEATHER_URL = "https://samples.openweathermap.org/data/2.5/forecast?q=%s" + "&appid=" + API_KEY;

}
