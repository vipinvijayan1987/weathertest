package com.example.weather_test;

public class WeatherItem {

    private String main, description, windSpeed;

    public WeatherItem() {
    }

    public WeatherItem(String main, String description, String windSpeed) {
        this.main = main;
        this.description = description;
        this.windSpeed = windSpeed;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String name) {
        this.main = name;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}