package com.example.weather_test;

import android.text.Html;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import static com.example.weather_test.Utils.ifNougat;

public class WeatherListAdapter extends RecyclerView.Adapter<WeatherListAdapter.MyViewHolder> {

    private List<WeatherItem> weatherItemList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;

        public MyViewHolder(TextView view) {
            super(view);
            title = view;
        }
    }

    public WeatherListAdapter(List<WeatherItem> weatherItemList) {
        this.weatherItemList = weatherItemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView textView = new TextView(parent.getContext());
        return new MyViewHolder(textView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        WeatherItem weatherItem = weatherItemList.get(position);
        String text = getFormattedText(weatherItem);
        holder.title.setText(ifNougat() ? Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(text));
    }

    @Override
    public int getItemCount() {
        return weatherItemList.size();
    }

    public String getFormattedText(WeatherItem weatherItem) {
        return "<h1>" + weatherItem.getMain() + "</h1><h2>" + weatherItem.getDescription() + "</h2>Wind Speed" + weatherItem.getWindSpeed() + "<br /><hr />";
    }

}