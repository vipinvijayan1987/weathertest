package com.example.weather_test;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Handler.Callback {

    private List<WeatherItem> weatherItemList;
    private WeatherListAdapter weatherListAdapter;

    private RecyclerView weatherListView;
    private Button btnSearch;
    private ProgressBar progressBar;
    private EditText edtSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("WEATHER");
        findViews();
        initList();
        showPb(false);
        enableList(false);
    }

    private void findViews() {
        weatherListView = findViewById(R.id.weatherListView);
        btnSearch = findViewById(R.id.btnSearch);
        progressBar = findViewById(R.id.pb);
        edtSearch = findViewById(R.id.edtSearch);
        btnSearch.setOnClickListener(this);
    }

    private void initList() {
        weatherItemList = new ArrayList<>();
        weatherListAdapter = new WeatherListAdapter(weatherItemList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        weatherListView.setLayoutManager(mLayoutManager);
        weatherListView.setItemAnimator(new DefaultItemAnimator());
        weatherListView.setAdapter(weatherListAdapter);
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {
        showPb(false);
        if (null == msg || null == msg.obj) {
            Utils.showError(MainActivity.this, "NO WEATHER DATA");
            return true;
        }
        setListData(msg);
        setTitle("WEATHER [" + weatherItemList.size() + "]");
        return false;
    }

    private void setListData(@NonNull Message msg) {
        List<WeatherItem> list = (List<WeatherItem>) msg.obj;
        weatherItemList.clear();
        weatherItemList.addAll(list);
        weatherListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        Utils.hideKeyboard(MainActivity.this);
        if (v == btnSearch) {
            setTitle("WEATHER");
            String location = edtSearch.getText().toString();
            if (null == location || location.trim().isEmpty()) {
                Utils.showError(MainActivity.this, "Please enter a location to search");
                return;
            }
            showPb(true);
            Service.getInstance().getData(getApplicationContext(), location, new Handler(this));
        }
    }

    public void enableList(boolean enable) {
        if (null == weatherListView) {
            return;
        }
        weatherListView.setEnabled(enable);
    }

    public void showPb(boolean visible) {
        if (null == progressBar) {
            return;
        }
        progressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
    }


}
